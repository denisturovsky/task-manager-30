package ru.tsc.denisturovsky.tm.exception.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.exception.AbstractException;

@NoArgsConstructor
public class AbstractSystemException extends AbstractException {

    public AbstractSystemException(@NotNull String message) {
        super(message);
    }

    public AbstractSystemException(
            @NotNull String message,
            @NotNull Throwable cause
    ) {
        super(message, cause);
    }

    public AbstractSystemException(@NotNull Throwable cause) {
        super(cause);
    }

    public AbstractSystemException(
            @NotNull String message,
            @NotNull Throwable cause,
            boolean enableSuppression,
            boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
